import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {AppMenu} from "./component/AppMenu";
import {BrowserRouter as Router, Route} from "react-router-dom";
import {SearchPage} from "./component/SearchPage";
import {UserPage} from "./component/UserPage";

class App extends Component {
  render() {
    return (
        <div>
          <AppMenu/>
          <Router>
            <Route path={"/search"} component={SearchPage}/>
            <Route path={"/view/:userId"} component={UserPage}/>
          </Router>
        </div>
    );
  }
}

export default App;
