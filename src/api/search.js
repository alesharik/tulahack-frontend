import {SERVER} from "./server";

export function search(abilities, extensions) {
    return fetch(SERVER + "/api/search", {
        method: "POST",
        body: JSON.stringify({abilities: abilities, extensions: extensions})
    }).then(value => {
        return value.json()
    });
}