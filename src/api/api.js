import {SERVER} from "./server";

export function getAbilities() {
    return fetch(SERVER + "/global/abilities")
        .then(value => value.json())
}

export function getUser(id) {
    return fetch(SERVER + "/admin/user/info?id=" + id)
        .then(value => value.json())
}