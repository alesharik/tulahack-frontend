import React, {Component} from 'react'
import {Button, Grid} from "semantic-ui-react";
import {Form, Dropdown} from "semantic-ui-react";
import {Link} from "react-router-dom";
import {getAbilities} from "../api/api";
import {search} from "../api/search";
import ReactTable from "react-table";

class TableElement {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}

let globalState = {humans: [], data: []};

export class SearchPage extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {abilities: [], entries: [], loading: true, request: null, data: globalState.data, humans: globalState.humans};
        getAbilities().then(value => {
            return value.map((e) => e.name).map((e) => {return {key: e,text: e, value: e}})
        })
            .then(value => {
                let s = this.state;
                s.abilities = value;
                this.setState(s);
            });
        this.handleChange = this.handleChange.bind(this);
        if(this.state.data.length === 0) {
            search([], []).then(value => {
                let state = this.state;
                state.humans = value
                    .map((e) => new TableElement(e.id, e.name));
                state.loading = false;
                globalState.humans = state.humans;
                this.setState(state);
            });
        } else
            this.state.loading = false;
    }

    handleChange = (e, { value }) => {
        let state = this.state;
        state.data = value;
        globalState.data = value;
        state.loading = true;
        this.setState(state);

        let abilities = [];
        value.forEach(value1 => {
            if(!value1.empty) {
                abilities.push({ability: value1, hours: -1, notes: -1})
            }
        });

        search(abilities, []).then(value => {
            let state = this.state;
            state.humans = value.map((e) => new TableElement(e.id, e.name));
            state.loading = false;
            globalState.humans = state.humans;
            this.setState(state);
        });
    };


    render() {
        return (<Grid>
            <Grid.Row>
                <Grid.Column width={16}>
                    <Form>
                        <Form.Field>
                            <label>Tags: </label>
                            <Dropdown placeholder={"react javascript..."} fluid multiple search selection options={this.state.abilities} onChange={this.handleChange} value={this.state.data}/>
                        </Form.Field>
                    </Form>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                <Grid.Column width={16}>
                    <ReactTable loading={this.state.loading} data={this.state.humans} columns={[{id: "col1", Header: "Name", Cell: props => {
                            return <Link to={"/view/" + props.row._original.id}>{props.value}</Link>;
                        }, accessor: d => d.name}]}/>
                </Grid.Column>
            </Grid.Row>
        </Grid>);
    }
}