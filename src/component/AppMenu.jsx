import React, {Component} from 'react';
import {Menu, MenuItem} from 'semantic-ui-react';
import logo from './logo.svg'
import {Link} from "react-router-dom";

export class AppMenu extends Component {

    render() {
        return (<Menu>
            <MenuItem>
                <img src={logo}/>
            </MenuItem>
            <MenuItem>
                <a href={"/search"}>Search</a>
            </MenuItem>
        </Menu>)
    }
}
