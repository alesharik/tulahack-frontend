import React, {Component} from 'react';
import {getUser} from "../api/api";
import {Grid} from "semantic-ui-react";
import {Header, Segment, Table, Label, Image, Rating} from "semantic-ui-react";
import admin from './admin.svg';
import hr from './hr.svg';
import user from './user.svg';

export function rating(notes) {
    let sum = 0;
    notes.forEach((n) => {
        let num = 0;
        if(n.note === "ONE")
            num = 1;
        else if(n.note === "TWO")
            num = 2;
        else if(n.note === "THREE")
            num = 3;
        else if(n.note === "FOUR")
            num = 4;
        else if(n.note === "FIVE")
            num = 5;
        sum += num;
    });
    return sum / notes.length;
}

export class UserPage extends Component {

    constructor(props, context) {
        super(props, context);
        const {match: {params}} = this.props;
        getUser(params.userId).then(value => {
            this.setState(value);
        });
        this.state = {login: "", name: "", abilities: [], contacts: [], stats: [], integrations: []};
    }

    render() {
        return (<div>
            <Header as={"h2"}>
                <Image circular src={this.state.role == "ADMIN" ? admin : (this.state.role == "HUMAN_RESOURCE" ? hr : user)}/>{this.state.name}
            </Header>
            <Segment color={"green"}>
                <Label as='a' color='green' ribbon>
                    General
                </Label>
                <Table definition>
                    <Table.Body>
                        <Table.Row>
                            <Table.Cell>Login</Table.Cell>
                            <Table.Cell>{this.state.login}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>Role</Table.Cell>
                            <Table.Cell>{this.state.role}</Table.Cell>
                        </Table.Row>
                        <Table.Row>
                            <Table.Cell>Abilities</Table.Cell>
                            <Table.Cell>{this.state.abilities.map(value => {
                                return <Label style={{backgroundColor: value.type.color}} key={value.type.id}>{value.name}</Label>
                            })
                            }</Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
            </Segment>
            {this.state.contacts.length === 0 ? <div/> : (
                <Segment color={"yellow"}>
                    <Label as='a' color='yellow' ribbon>
                        Contacts
                    </Label>
                    <Table definition>
                        <Table.Body>
                            {this.state.contacts.map((c) => {
                                return <Table.Row>
                                    <Table.Cell>{c.service}</Table.Cell>
                                    <Table.Cell>{c.address}</Table.Cell>
                                </Table.Row>
                            })}
                        </Table.Body>
                    </Table>
                </Segment>
            )}
            {this.state.stats.length === 0 ? <div/> : (
            <Segment color={"blue"}>
                <Label as='a' color='blue' ribbon>
                    Statistics
                </Label>
                <Table definition>
                    <Table.Body>
                        {this.state.stats.map((c) => {
                            return <Table.Row>
                                <Table.Cell> <Label style={{backgroundColor: c.ability.type.color}} key={c.ability.type.id}>{c.ability.name}</Label></Table.Cell>
                                <Table.Cell>{c.hours + " hrs"}</Table.Cell>
                                <Table.Cell> <Rating icon='star' defaultRating={rating(c.notes)} maxRating={5} disabled/></Table.Cell>
                            </Table.Row>
                        })}
                    </Table.Body>
                </Table>
            </Segment>
            )}
            {this.state.integrations.length === 0 ? <div/> : (
            <Segment color={"red"}>
                <Label as='a' color='red' ribbon>
                    Integrations
                </Label>
                <Table definition>
                    <Table.Body>
                        {this.state.integrations.map((c) => {
                            return c.fields.map((a) => {
                                return <Table.Row>
                                    <Table.Cell>{c.integrationUid + a.key}</Table.Cell>
                                    <Table.Cell>{a.value}</Table.Cell>
                                </Table.Row>
                            });
                        })}
                    </Table.Body>
                </Table>
            </Segment>
        )}
        </div>)
    }
}